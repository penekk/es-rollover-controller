# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.5.0] - 2021-02-14

### Added

- fix a bug where index patterns could match multiple indices ([see #29](https://gitlab.com/msvechla/es-rollover-controller/-/issues/29)). This will adapt all index template patterns by creating an array of matching patterns by appending the following regex to the existing pattern: `[0-9]*`. This will not affect the current behaviour, as indices anyways have to end with the increment pattern suffix
- add `rollover_index_annotation_key` support to helm chart
- fix wrong elastic API usage for cleanup of legacy indices

## [0.4.0] - 2021-02-01

### Added

- support for specifying rollover indices via annotations ([see #28](https://gitlab.com/msvechla/es-rollover-controller/-/issues/28))

### Changed

- update module ReneKroon/ttlcache to v2
- update module elastic/go-elasticsearch/v7 to v7.10.0
- update docker image to latest alpine and go

## [0.3.0] - 2021-01-11

### Added

- support for date math syntax in ILM Format ([see #25](https://gitlab.com/msvechla/es-rollover-controller/-/issues/25))

## [0.2.2] - 2020-12-23

### Fixed

- sensitive config options are now redacted in the logs ([see #26](https://gitlab.com/msvechla/es-rollover-controller/-/issues/26))
- upgrade to latest alpine base image

## [0.2.1] - 2020-09-29

### Fixed

- ttl cache is now invalidated based on ILM policy as well ([see #24](https://gitlab.com/msvechla/es-rollover-controller/-/issues/24))

## [0.2.0] - 2020-08-30

### Added 

- new config option `CONTROLLER_ID` to specify identity of the controller
- support for new `_index_template` v2 API
- v2 index templates now include metadata with controller ownership info
- config option `CLEANUP_LEGACY_INDEX_TEMPLATES` to remove outdated index templates when running against an elasticsearch cluster with v2 support
- support for garbage collection of stale rollover controller artefacts, which cleans up index templates if they are no longer managed by the controller
- new flag `GARBAGE_COLLECTION_INTERVAL` to control the interval at which the garbage collection should run
- running tests against multiple elasticsearch versions

### Changed

- upgraded elasticsearch client to v7.8.0

## [0.1.1] - 2020-06-25

### Fixed

- use system cert pool as base certificate pool as until now an empty pool was used, which could cause issues with trusted certificates (PR by @hieu.le6 👏 / see [!9](https://gitlab.com/msvechla/es-rollover-controller/-/merge_requests/9))

## [0.1.0] - 2020-05-11

### Added

- new option `ILM_POLICY_LABEL_KEY`, which allows attaching custom ILM policies to the index template of specific rollover indices based on a pod label ([see #20](https://gitlab.com/msvechla/es-rollover-controller/-/issues/20))
- updated Helm chart with new option
- improved end to end tests

### Fixed

- state of index templates controlled by the controller is now ensured with improved logic. Before it was only verified if the template exists, now the various settings are ensured as well

## [0.0.9] - 2020-05-02

### Fixed

- update go to 1.14
- update client-go to 1.17.5
- updated all go dependencies to get the latest bug and vulnerability fixes
- update helm and snyk-cli to latest version
- bumped elasticsearch e2e test docker image to 7.6.2

## [0.0.8] - 2019-12-28

### Fixed

- updated all go dependencies to get the latest bug and vulnerability fixes

## [0.0.8] - 2019-12-28

### Fixed

- updated all go dependencies to get the latest bug and vulnerability fixes

## [0.0.7] - 2019-12-17

### Added

- option `elasticsearchTimeout` to specify elasticsearch dialer and response header timeout
- option `workers` to specify the number of worker threads to process kubernetes events
- updated helm chart with new options accordingly

## [0.0.6] - 2019-12-16

### Fixed

- fix an issue where a non-matching index pattern was configured when a custom ILMFormat has been set

## [0.0.5] - 2019-12-12

### Fixed

- fix nilpointer during es bootstrap when elasticsearch is unavailable ([see #6](https://gitlab.com/msvechla/es-rollover-controller/issues/6))

### Added

- implement new TTL Cache for periodically re-ensuring rollover setup for discovered indices ([see #7](https://gitlab.com/msvechla/es-rollover-controller/issues/7))
- added new option `cacheTTL` to helm chart

## [0.0.4] - 2019-10-20

### Breaking Changes

- renamed `ilmPattern` to `ilmFormat` to allow more fined-grained control, default value is unchanged
- renamed `indexTemplateSuffix` to `indexTemplateFormat` to allow more fined-grained control, default value is unchanged
- renamed `writeAliasSuffix` to `writeAliasFormat` to allow more fined-grained control, default value is unchanged

### Added

- service account with least privileges for the controller
- `--namespace_scope` option to specify the scope of the controller (namespaced or cluster-wide)

### Fixed

- setting the `--rollover_index_label_key` option had no effect in past releases

## [0.0.3] - 2019-10-15

### Added

- allow configuration of log format (json or text)
- helm lint step for CI

### Fixed

- adapted CI to push docker images before releasing helm chart

## [0.0.2] - 2019-10-14

### Added

- allow configuration of custom `ilmPattern`

## [0.0.1] - 2019-10-13

### Added

- Initial pre-release of the controller
- Initial release of the matching helm chart
- Initial docker image release
- Quickstart Guide
- Gitlab Page hosting the Helm Repository and a static Readme
- Lots more 🚀
