#!/usr/bin/env bash
# Updates the Helm charts README.md and packages the helm chart version.
# This script should be executed from the project root after a new Helm chart release.

echo "Ensure you have bumped the controller version, the Helm Chart version and the docker image tag of the Helm Chart before releasing a new Chart"
echo ""
read -p "Press enter to continue"

cd deploy/helm/es-rollover-controller/
helm-docs

cd repository
mkdir new
helm package ../../es-rollover-controller/ -d new/
helm repo index --url https://msvechla.gitlab.io/es-rollover-controller --merge index.yaml new/
mv new/* .
rm -rf new/