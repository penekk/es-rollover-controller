## Builder Image
FROM golang:1 as builder
RUN apt update && apt install -y --no-install-recommends --upgrade openssl
WORKDIR /es-rollover-controller
# Copy go mod first and download all modules so layers will be cached for faster local builds
COPY go.mod .
COPY go.sum .
RUN go mod download

COPY / .
RUN CGO_ENABLED=0 GO111MODULE=on GOOS=linux go build -a -installsuffix cgo -o es-rollover-controller .

## Application Image
FROM alpine:latest

RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

RUN mkdir -p /home/app && \
    addgroup app && \
    adduser -D -G app app

WORKDIR /home/app
COPY --from=builder /es-rollover-controller/es-rollover-controller .
RUN chown -R app:app /home/app
USER app
ENTRYPOINT ["./es-rollover-controller"]
