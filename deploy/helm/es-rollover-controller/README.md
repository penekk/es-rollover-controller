# es-rollover-controller

A Helm chart for deploying https://gitlab.com/msvechla/es-rollover-controller

A Kubernetes Controller for dynamically creating Elasticsearch Write Aliases, Rollover Indices and Index Lifecycle Management Policies based on Pod Labels.

Current Version: `0.5.0`

## Getting Started

To get started with the defaul configuration, simply install the helm chart from the repository:

```sh
helm repo add es-rollover-controller https://msvechla.gitlab.io/es-rollover-controller/
helm install es-rollover-controller/es-rollover-controller
```

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| cacheTTL | string | `"12h"` | TTL of cached annotations values that have already been discovered. |
| customILMPolicy.configMapKey | string | `""` | Key inside an existing configmap which contains a custom ILM policy. |
| customILMPolicy.configMapName | string | `""` | Name of an existing configmap containing a custom ILM policy. |
| elasticsearch.caCertFile | string | `"/usr/share/elasticsearch-certs/tls.crt"` |  |
| elasticsearch.caCertSecretName | string | `""` |  |
| elasticsearch.host | string | `"http://elastic"` | The elasticsearch host. |
| elasticsearch.password.raw | string | `""` | Elasticsearch password, will create a matching Kubernetes secret. Alternatively specify `elasticsearch.password.secretKeyRef`. |
| elasticsearch.password.secretKeyRef.key | string | `""` | Key inside an existing secret which contains the elasticsearch password. `elasticsearch.password.raw`. |
| elasticsearch.password.secretKeyRef.name | string | `""` | Name of an existing secret containing the elasticsearch password. Alternatively specify `elasticsearch.password.raw`. |
| elasticsearch.port | int | `9200` | The elasticsearch port. |
| elasticsearch.timeout | string | `"30s"` |  |
| elasticsearch.username | string | `""` | The elasticsearch username. |
| fullnameOverride | string | `""` |  |
| ilmFormat | string | `"%s-000001"` | Format String of the rollover index, %s will be substituted with the index name. |
| ilmPolicyLabelKey | string | `"elastic-ilm-policy"` | Optional key of the pod label that contains the name of an existing custom ILM policy that should be used for the index template. If not specified, the default ILM policy will be used. |
| ilmPolicyName | string | `"es-rollover-controller"` | Name of the ILM policy that should be used. |
| ilmPolicyOverwrite | bool | `false` | Specifies whether the lifecycle policy is overwritten at startup. |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.repository | string | `"msvechla/es-rollover-controller"` |  |
| image.tag | string | `"0.5.0"` |  |
| imagePullSecrets | list | `[]` |  |
| indexTemplateFormat | string | `"%s_es-rollover-controller"` | Format String of the index template, %s will be substituted with the index name.. |
| log.formatter | string | `"json"` | The log formatter. Should be one of [json, text]. |
| log.rolloverIndexLabelValue | string | `"es-rollover-controller"` | The rolloverIndexLabel to set on the controller itself to forward and rollover its logs. If empty, no label will be set. |
| nameOverride | string | `""` |  |
| namespaceScope | string | `""` |  |
| nodeSelector | object | `{}` |  |
| replicaCount | int | `1` |  |
| resources.limits.cpu | string | `"100m"` |  |
| resources.limits.memory | string | `"128Mi"` |  |
| resources.requests.cpu | string | `"100m"` |  |
| resources.requests.memory | string | `"128Mi"` |  |
| rolloverIndexAnnotationKey | string | `""` | Key of the pod annotation that contains the name of the rollover index. Usage of rollover_index_label_key preferred for performance reasons, see README for more info. |
| rolloverIndexLabelKey | string | `"elastic-index"` | Key of the pod label that contains the name of the rollover index. |
| tolerations | list | `[]` |  |
| vpa.enabled | bool | `false` | Whether a vertical pod autoscaling CRD should be created for the controller |
| vpa.updateMode | string | `"Auto"` | The update mode for the vertical pod autoscaler |
| workers | int | `2` | Number of workers that will process kubernetes events. |
| writeAliasFormat | string | `"%s_writealias"` | Format String of the write alias, %s will be substituted with the index name. |
