package main

import (
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/pkg/errors"
	coreinformers "k8s.io/client-go/informers/core/v1"
	"k8s.io/client-go/tools/cache"
	"mariussvechla.com/es-rollover-controller/internal/pkg/config"
	"mariussvechla.com/es-rollover-controller/internal/pkg/esutil"
	"mariussvechla.com/es-rollover-controller/internal/pkg/kubeutil"
)

// startGarbageCollector starts the background task for cleaning up stale rollover artefacts
func startGarbageCollector(informer coreinformers.PodInformer, esClient *esutil.ESClient, c *config.Config, stopCh <-chan struct{}, gcInterval time.Duration) *chan bool {
	ticker := time.NewTicker(gcInterval)
	done := make(chan bool)

	go func() {

		if ok := cache.WaitForCacheSync(stopCh, informer.Informer().HasSynced); !ok {
			log.Errorf("failed to wait for caches to sync for garbage collection")
			ticker.Stop()
			return
		}
		for {
			select {
			case <-done:
				ticker.Stop()
				return
			case <-ticker.C:
				_, err := cleanupStaleRolloverIndexArtefacts(informer, esClient, c)
				if err != nil {
					log.Errorf("cleaning up rollover index artefacts: %s", err)
				}
			}
		}
	}()
	return &done
}

// cleanupStaleRolloverIndexArtefacts deletes artefacts in elasticsearch, that are no longer manged by the controller. Returns a list of all delete index template names
func cleanupStaleRolloverIndexArtefacts(informer coreinformers.PodInformer, esClient *esutil.ESClient, c *config.Config) ([]string, error) {
	log.Info("starting garbage collection run")

	managedIndexTemplates, err := esClient.FindManagedIndexTemplates()
	if err != nil {
		return nil, errors.Wrap(err, "retrieving managed index templates")
	}

	managedLabelVals, err := kubeutil.ListManagedRolloverIndexValues(informer, c)
	if err != nil {
		return nil, errors.Wrap(err, "retrieving managed rollover index label values")
	}

	var deletedArtefacts []string

	// remove all index templates that are no longer managed
	for _, t := range managedIndexTemplates {
		stillManaged := false
		for _, l := range managedLabelVals {
			if t == esClient.GetIndexTemplateName(l) {
				stillManaged = true
				break
			}
		}

		if stillManaged {
			continue
		}

		err := esClient.DeleteIndexTemplate(t)
		if err != nil {
			return nil, errors.Wrapf(err, "deleting stale index template %s", t)
		}

		deletedArtefacts = append(deletedArtefacts, t)
		log.Infof("cleaned up stale index template %s", t)
	}

	log.Info("finished garbage collection run")
	return deletedArtefacts, nil
}
