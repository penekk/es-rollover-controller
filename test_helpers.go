package main

import (
	"context"
	"fmt"
	"io"
	"math/rand"
	"os"
	"strconv"
	"testing"
	"time"

	docker "docker.io/go-docker"
	"docker.io/go-docker/api/types"
	"docker.io/go-docker/api/types/container"
	"docker.io/go-docker/api/types/network"
	"github.com/docker/go-connections/nat"
	"github.com/elastic/go-elasticsearch/v7"
	"github.com/stretchr/testify/assert"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	kubeinformers "k8s.io/client-go/informers"
	k8sfake "k8s.io/client-go/kubernetes/fake"
	"k8s.io/client-go/tools/record"
	"mariussvechla.com/es-rollover-controller/internal/pkg/config"
)

var (
	esDockerImages = []string{
		"docker.elastic.co/elasticsearch/elasticsearch:7.6.2",
		"docker.elastic.co/elasticsearch/elasticsearch:7.8.0",
	}
)

type fixture struct {
	t            *testing.T
	dockerClient *docker.Client

	kubeclient *k8sfake.Clientset
	// Objects to put in the store.
	podLister []*corev1.Pod
}

func newFixture(t *testing.T) *fixture {
	f := &fixture{}
	f.t = t

	cli, err := docker.NewEnvClient()
	if err != nil {
		panic(err)
	}

	f.dockerClient = cli
	return f
}

// newPod creates a pod with the specified labels and annotations
func newPod(name string, labels, annotations map[string]string) *corev1.Pod {
	return &corev1.Pod{
		TypeMeta: metav1.TypeMeta{APIVersion: corev1.SchemeGroupVersion.String()},
		ObjectMeta: metav1.ObjectMeta{
			Name:        name,
			Namespace:   metav1.NamespaceDefault,
			Labels:      labels,
			Annotations: annotations,
		},
		Spec: corev1.PodSpec{},
	}
}

// deletePodByName deletes a pod from a podlist by name
func deletePodByName(name string, pods []*corev1.Pod) []*corev1.Pod {
	for i, p := range pods {
		if p.Name == name {
			return append(pods[:i], pods[i+1:]...)
		}
	}
	return pods
}

// newController creates a new rollover controller with the specified config
func (f *fixture) newController(conf *config.Config) (*Controller, kubeinformers.SharedInformerFactory) {
	f.kubeclient = k8sfake.NewSimpleClientset()

	podInformerFactory := createRolloverIndexPodInformerFactory(f.kubeclient, conf.FlagRolloverIndexLabelKey, conf.FlagRolloverIndexAnnotationKey, conf.FlagNamespaceScope)

	done := make(chan struct{})
	c := NewController(f.kubeclient,
		podInformerFactory.Core().V1().Pods(), conf, done)

	c.recorder = &record.FakeRecorder{}

	for _, p := range f.podLister {
		err := podInformerFactory.Core().V1().Pods().Informer().GetIndexer().Add(p)
		assert.NoError(f.t, err)
	}

	return c, podInformerFactory
}

// runControlLoop runs the controller logic once and returns a slice of performed actions
func (f *fixture) runControlLoop(conf *config.Config, podName string) []string {
	c, i := f.newController(conf)
	stopCh := make(chan struct{})
	defer close(stopCh)
	i.Start(stopCh)

	err := c.syncHandler(podName)
	if err != nil {
		f.t.Errorf("error syncing pod: %v", err)
	}

	return c.esClient.PerformedActions
}

// createTestESInstance creates a elasticsearch instance in docker, exposes the ports and returns a config that connects to it
func (f *fixture) createTestESInstance(dockerImage string) (*config.Config, string, error) {
	esHost := "http://localhost"
	esHostEnv := os.Getenv("ES_HOST")

	if esHostEnv != "" {
		esHost = esHostEnv
	}

	conf := config.InitApp(true)
	conf.FlagESHost = esHost
	conf.FlagESPort = strconv.Itoa(9000 + rand.Intn(999))

	// pull the image
	reader, err := f.dockerClient.ImagePull(context.Background(), dockerImage, types.ImagePullOptions{})
	if err != nil {
		return nil, "", err
	}

	_, err = io.Copy(os.Stdout, reader)
	assert.NoError(f.t, err)

	_, exposedPorts, _ := nat.ParsePortSpecs([]string{
		fmt.Sprintf("0.0.0.0:%s:9200/tcp", conf.FlagESPort),
	})

	containerConfig := container.Config{
		Image: dockerImage,
		Env:   []string{"discovery.type=single-node"},
	}

	cont, err := f.dockerClient.ContainerCreate(context.Background(), &containerConfig, &container.HostConfig{PortBindings: exposedPorts}, &network.NetworkingConfig{}, fmt.Sprintf("testESInstance-%.3d", rand.Intn(999)))
	if err != nil {
		return nil, "", err
	}

	err = f.dockerClient.ContainerStart(context.Background(), cont.ID, types.ContainerStartOptions{})
	if err != nil {
		return nil, cont.ID, err
	}

	err = waitForES(esHost, conf.FlagESPort)
	if err != nil {
		return nil, cont.ID, err
	}

	return conf, cont.ID, nil
}

// waitForES waits for an elasticseach instance to become available
func waitForES(esHost, esPort string) error {

	cfg := elasticsearch.Config{
		Addresses: []string{
			fmt.Sprintf("%s:%s", esHost, esPort),
		},
	}

	client, err := elasticsearch.NewClient(cfg)
	if err != nil {
		return err
	}

	for {
		_, err = client.Ping()
		if err != nil {
			fmt.Printf("ES not yet ready: %s\n", err)
			time.Sleep(time.Second * 5)
		} else {
			break
		}
	}
	return nil
}

// cleanupTestESInstance removes an docker container by its ID for cleaning up test ressources
func (f *fixture) cleanupTestESInstance(containerID string) {
	if containerID != "" {
		err := f.dockerClient.ContainerRemove(context.Background(), containerID, types.ContainerRemoveOptions{Force: true})
		assert.NoError(f.t, err)
	}
}
