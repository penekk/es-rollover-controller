package kubeutil

import (
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/labels"
	coreinformers "k8s.io/client-go/informers/core/v1"
	"mariussvechla.com/es-rollover-controller/internal/pkg/config"
)

// ListManagedRolloverIndexValues returns a slice of all managed rollover index label and annotation values in the cluster
func ListManagedRolloverIndexValues(informer coreinformers.PodInformer, c *config.Config) ([]string, error) {
	podList, err := informer.Lister().List(labels.NewSelector())
	if err != nil {
		return nil, err
	}

	var managedRolloverVals []string

	for _, p := range podList {
		vals := GetRolloverIndexKVForPod(p, c)

		for _, val := range vals {
			managedRolloverVals = append(managedRolloverVals, val)
		}
	}
	return managedRolloverVals, nil
}

// GetRolloverIndexKVForPod returns a map of rollover key/value pairs from a pods annotations and labels
func GetRolloverIndexKVForPod(pod *corev1.Pod, c *config.Config) map[string]string {
	vals := map[string]string{}

	if val, exists := pod.GetAnnotations()[c.FlagRolloverIndexAnnotationKey]; exists {
		vals[c.FlagRolloverIndexAnnotationKey] = val
	}

	// label takes precedence over annotation if both specify the same key for backwards compatibility
	if val, exists := pod.GetLabels()[c.FlagRolloverIndexLabelKey]; exists {
		vals[c.FlagRolloverIndexLabelKey] = val
	}
	return vals
}
