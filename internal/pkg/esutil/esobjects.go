package esutil

import (
	"fmt"

	"github.com/Masterminds/semver"
)

type Meta struct {
	ManagedBy string `json:"managed_by,omitempty"`
}

type IndexSettings struct {
	LifecycleName string `json:"index.lifecycle.name"`
	RolloverAlias string `json:"index.lifecycle.rollover_alias"`
}

type Template struct {
	Settings *IndexSettings `json:"settings"`
}

type IndexTemplate struct {
	IndexPatterns []string       `json:"index_patterns"`
	Settings      *IndexSettings `json:"settings,omitempty"`
	Template      *Template      `json:"template"`
	Meta          *Meta          `json:"_meta,omitempty"`
}

type IsWriteIndex struct {
	IsWriteIndex bool `json:"is_write_index"`
}

type RolloverIndex struct {
	Aliases  map[string]*IsWriteIndex `json:"aliases"`
	Settings *IndexSettings           `json:"settings"`
}

// generateIndexSettings returns a string containing the index settings object
func (es *ESClient) generateIndexSettings(elasticIndexlabelValue string, customILMPolicyLabelValue string) *IndexSettings {
	return &IndexSettings{
		LifecycleName: es.GetILMPolicyName(customILMPolicyLabelValue),
		RolloverAlias: es.GetWriteAlias(elasticIndexlabelValue),
	}
}

// generateIndexTemplateBody returns a string containing the index template request body
func (es *ESClient) generateIndexTemplateBody(elasticIndexLabelValue string, customILMPolicyLabelValue string) (*IndexTemplate, error) {
	indexSettings := es.generateIndexSettings(
		elasticIndexLabelValue,
		customILMPolicyLabelValue,
	)

	meta := &Meta{
		ManagedBy: es.config.FlagControllerID,
	}

	template := &Template{
		Settings: indexSettings,
	}

	var rootSettings *IndexSettings = nil

	if !es.HasMetadataSupport() {
		meta = nil
		template = nil
		rootSettings = indexSettings
	}

	return &IndexTemplate{
		IndexPatterns: es.GetRolloverIndexPattern(elasticIndexLabelValue),
		Settings:      rootSettings,
		Template:      template,
		Meta:          meta,
	}, nil
}

func (es *ESClient) generateRolloverIndex(elasticIndexlabelValue string, customILMPolicyLabelValue string) *RolloverIndex {
	indexSettings := es.generateIndexSettings(elasticIndexlabelValue, customILMPolicyLabelValue)

	return &RolloverIndex{
		Aliases: map[string]*IsWriteIndex{
			es.GetWriteAlias(elasticIndexlabelValue): &IsWriteIndex{
				IsWriteIndex: true,
			},
		},
		Settings: indexSettings,
	}
}

// HasMetadataSupport indicates whether the es cluster supports adding metadata to index templates
func (es *ESClient) HasMetadataSupport() bool {
	vSupportsMetadata, err := semver.NewConstraint(">=7.8.0")

	if err != nil {
		panic(fmt.Sprintf("error parsing sever: %s", err))
	}

	return vSupportsMetadata.Check(es.Version)
}
