package esutil

import (
	"encoding/json"
	"testing"

	"github.com/Masterminds/semver"
	"github.com/stretchr/testify/assert"
	"mariussvechla.com/es-rollover-controller/internal/pkg/config"
)

func TestGenerateIndexSettings(t *testing.T) {

	cli := ESClient{
		config: &config.Config{
			FlagWriteAliasFormat: "%s_esrc",
		},
	}

	testCases := []struct {
		labelValue     string
		version        *semver.Version
		expectedOutput *IndexSettings
	}{
		{
			labelValue: "helloWorld",
			version:    semver.MustParse("7.8.0"),
			expectedOutput: &IndexSettings{
				LifecycleName: "helloWorld",
				RolloverAlias: "helloWorld_esrc",
			},
		},
		{
			labelValue: "testing",
			version:    semver.MustParse("6.7.0"),
			expectedOutput: &IndexSettings{
				LifecycleName: "testing",
				RolloverAlias: "testing_esrc",
			},
		},
	}

	for _, c := range testCases {
		cli.Version = c.version
		actualOutput := cli.generateIndexSettings(c.labelValue, c.labelValue)

		assert.Equal(t, c.expectedOutput, actualOutput)
		_, err := json.Marshal(actualOutput)
		assert.NoError(t, err)
	}
}

func TestGenerateIndexTemplateBody(t *testing.T) {

	cli := ESClient{
		config: &config.Config{
			FlagWriteAliasFormat:    "%s_esrc",
			FlagIndexTemplateFormat: "%s_esrc",
			FlagILMFormat:           "%s_esrc",
		},
	}

	testCases := []struct {
		labelValue     string
		version        *semver.Version
		expectedOutput *IndexTemplate
	}{
		{
			labelValue: "helloWorld",
			version:    semver.MustParse("7.8.0"),
			expectedOutput: &IndexTemplate{
				IndexPatterns: []string{"helloWorld_esrc-0*", "helloWorld_esrc-1*", "helloWorld_esrc-2*", "helloWorld_esrc-3*", "helloWorld_esrc-4*", "helloWorld_esrc-5*", "helloWorld_esrc-6*", "helloWorld_esrc-7*", "helloWorld_esrc-8*", "helloWorld_esrc-9*"},
				Template: &Template{
					Settings: &IndexSettings{
						LifecycleName: "helloWorld",
						RolloverAlias: "helloWorld_esrc",
					},
				},
				Meta: &Meta{
					ManagedBy: "es-rollover-controller",
				},
			},
		},
		{
			labelValue: "testing",
			version:    semver.MustParse("6.7.0"),
			expectedOutput: &IndexTemplate{
				IndexPatterns: []string{"testing_esrc-0*", "testing_esrc-1*", "testing_esrc-2*", "testing_esrc-3*", "testing_esrc-4*", "testing_esrc-5*", "testing_esrc-6*", "testing_esrc-7*", "testing_esrc-8*", "testing_esrc-9*"},
				Settings: &IndexSettings{
					LifecycleName: "testing",
					RolloverAlias: "testing_esrc",
				},
			},
		},
	}

	for _, c := range testCases {
		cli.Version = c.version
		cli.config.FlagControllerID = "es-rollover-controller"

		actualOutput, err := cli.generateIndexTemplateBody(c.labelValue, c.labelValue)
		assert.NoError(t, err)
		assert.Equal(t, c.expectedOutput, actualOutput)

		_, err = json.Marshal(actualOutput)
		assert.NoError(t, err)
	}
}

func TestHasMetadataSupport(t *testing.T) {
	testCases := []struct {
		version                   *semver.Version
		shouldHaveMetadataSupport bool
	}{
		{version: semver.MustParse("6.7.7"), shouldHaveMetadataSupport: false},
		{version: semver.MustParse("7.7.0"), shouldHaveMetadataSupport: false},
		{version: semver.MustParse("7.8.0"), shouldHaveMetadataSupport: true},
	}

	esClient := ESClient{config: &config.Config{}}

	for _, c := range testCases {
		esClient.Version = c.version
		actualOutput := esClient.HasMetadataSupport()
		assert.Equal(t, c.shouldHaveMetadataSupport, actualOutput)
	}
}
